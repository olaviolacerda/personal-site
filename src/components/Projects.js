import React from 'react';
import { Section, Columns } from "react-bulma-components/full";
import Project from './Project'

const ProjectsSection = () => {
    return <Section>
        <Columns>
            <Columns.Column>
                <Project
                    title="PF das Ruas"
                    description="Donation of a website to the Volunteer Group"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'node', 'bulma', 'express']} />
            </Columns.Column>
            <Columns.Column>
                <Project
                    title="PF das Ruas"
                    description="Donation of a website to the Volunteer Group"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'node', 'bulma', 'express']} />
            </Columns.Column>
            <Columns.Column>
                <Project
                    title="PF das Ruas"
                    description="Donation of a website to the Volunteer Group"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'node', 'bulma', 'express']} />
            </Columns.Column>
            <Columns.Column>
                <Project
                    title="Pablo Gravações"
                    description="Website"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'heroku']} />
            </Columns.Column>
            <Columns.Column>
                <Project
                    title="PF das Ruas"
                    description="Donation of a website to the Volunteer Group"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'node', 'bulma', 'express']} />
            </Columns.Column>
            <Columns.Column>
                <Project
                    title="Pablo Gravações"
                    description="Website"
                    img="https://bulma.io/images/placeholders/1280x960.png"
                    to="https://www.google.com"
                    tecs={['react', 'sass', 'heroku']} />
            </Columns.Column>
        </Columns>
    </Section>;
}

export default ProjectsSection