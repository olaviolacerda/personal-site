import React from 'react'
import { Heading, Section, Content } from 'react-bulma-components/full';

const HeaderSection = () => {
    return (
        <Section>
            <Content>
                <Heading>Hello, I'm Olavio Lacerda</Heading>
                <Heading subtitle size={5}>
                    I'm a Fullstack Web Developer, currently working at <a href="https://www.realnetworking.co">Real Networking</a>.
                I have a Bachelor of Science in Computer Science, working with <span>Ruby on Rails</span> and <span>ReactJS</span> to build amazing things.
                            I too care about the design of the webpages, carring about the best concepts of UI/UX.
                            If you would like to work with me, let me know 🙂.
                </Heading>
            </Content>
        </Section>)
}

export default HeaderSection