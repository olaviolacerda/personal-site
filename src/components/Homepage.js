import React, { Component } from 'react';
import { Container } from "react-bulma-components/full";
import HeaderSection from './Header'
import ProjectsSection from './Projects';
import FooterSection from './Footer';
import './styles.scss'

export default class components extends Component {
    render() {
        return <Container fluid>
            <HeaderSection />
            <ProjectsSection />
            <FooterSection />
        </Container>;
    }
}
