import React from 'react'
import { Card, Media, Content, Heading, Tag } from "react-bulma-components/full";
import { FiTag } from 'react-icons/fi';
import PropTypes from 'prop-types';

const Project = ({ title, description, img, to, tecs }) => {
    return (
        <Card>
            <a href={to} rel="noopener noreferrer" target="_blank">
                <Card.Image src={img} alt="work image" />
                <Card.Content>

                    <Media>
                        <Media.Item>
                            <Heading size={6}>{title}</Heading>
                            <Heading subtitle size={6}>{description}</Heading>
                        </Media.Item>
                    </Media>
                    <Content>
                        <Tag.Group>
                            {tecs.map(tec => <Tag ><FiTag />{tec}</Tag>)}
                        </Tag.Group>
                    </Content>
                </Card.Content>
            </a>
        </Card>
    )
}

Project.propTypes = {
    tecs: PropTypes.array
};

Project.defaultProps = {
    tecs: []
};

export default Project;


