import React from 'react'
import { Icon } from "react-bulma-components/full";

const Tag = ({ title }) => {
    return (
        <li><span>{title}</span></li>
    )
}

export default Tag